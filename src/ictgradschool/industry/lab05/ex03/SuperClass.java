package ictgradschool.industry.lab05.ex03;

public class SuperClass {

    public int x = 10;
    static int y = 10;

    SuperClass() {
        x = y++; // y++ increments y BUT does not yet assign the value to y. The value of y BEFORE increment gets assigned to x which is 10.
    }

    public int foo() {
        return x;
    }

    public static int goo() {
        return y;
    }

}
