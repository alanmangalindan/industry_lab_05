package ictgradschool.industry.lab05.ex03;

public class Test2 extends SuperClass {

    static int x = 15;
    static int y = 15;
    int x2 = 20;
    static int y2 = 20;

    Test2() {
            x2 = y2++;
    }

    public int foo2() {
        return x2;
    }

    public static int goo2() {
        return y2;
    }

    public static int goo() {
        return y2;
    }

    public static void main(String[] args) {

        SuperClass s2 = new Test2();

        System.out.println("\nThe static Binding");
        System.out.println("S2.x = " + s2.x); // 10
        System.out.println("S2.y = " + s2.y); // 11
        System.out.println("S2.foo() = " + s2.foo()); // 10
        System.out.println("S2.goo() = " + s2.goo()); // 11

        System.out.println("Test2.y = " + Test2.y); // 15
        System.out.println("SuperClass.y = " + SuperClass.y); // 11

        System.out.println("((Test2) s2).foo2() = " + ((Test2) s2).foo2()); //to access foo2() from variable s2
        System.out.println("((Test2) s2).x2 = " + ((Test2) s2).x2); // to access x2 from variable s2

//        Test2 t2 = new SuperClass(); // compile error

//        Test2 t3 = (Test2) new SuperClass(); // ClassCastException - SuperClass cannot be cast to Test2


    }

}
