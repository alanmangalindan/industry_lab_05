package ictgradschool.industry.lab05.ex02;

/**
 * Represents a dog.
 *
 * TODO Make this class implement the IAnimal interface, then implement all its methods.
 */
public class Dog implements IAnimal {

    @Override
    public boolean isMammal() {
        return true;
    }

    @Override
    public int legCount() {
        return 4;
    }

    @Override
    public String myName() {
        return "Bruno the dog";
    }

    @Override
    public String sayHello() {
        return "woof woof";
    }
}
